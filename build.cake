#addin "Cake.Json"
#addin "Cake.Npm"
#addin "Cake.Git"


var target = Argument("target", "__NpmInstall");

Setup(context =>
{
    Information(@"              ___             _                                      _"); 
    Information(@"             |__ \           | |                                    (_)");
    Information(@"  _ __   __ _   ) |_____ __ _| |__   __ _  ___ _   _ ___  __ _ _ __  _ ");
    Information(@" | '_ \ / _` | / /______/ _` | '_ \ / _` |/ __| | | / __|/ _` | '_ \| |");
    Information(@" | | | | (_| |/ /_     | (_| | |_) | (_| | (__| |_| \__ \ (_| | |_) | |");
    Information(@" |_| |_|\__, |____|     \__,_|_.__/ \__,_|\___|\__,_|___/\__,_| .__/|_|");
    Information(@"         __/ |                                                | |      ");
    Information(@"        |___/                                                 |_|      ");
    

});
Task("__Versioning")
    .Does(() => {
		TeamCity.WriteStartBlock("Build number versioning");

		var packageFile = "./package.json";

        var deserializedPackage = ParseJsonFromFile(packageFile);
		TeamCity.SetBuildNumber(deserializedPackage["version"].ToString());

        TeamCity.WriteEndBlock("Build number versioning");
	});

Task("__NpmInstall")
    .Does(() => {
        TeamCity.WriteStartBlock("Npm Install");
        Npm.Install();
        Npm.RunScript("afterinstall");
        TeamCity.WriteEndBlock("Npm Install");
    });

Task("__Build")
    .Does(() => {
        TeamCity.WriteStartBlock("Build");
        Npm.RunScript("beforepublish");
        TeamCity.WriteEndBlock("Build");
    });

Task("__Test")
    .Does(() => {
        TeamCity.WriteStartBlock("Test");
        Npm.RunScript("testme");
        TeamCity.WriteEndBlock("Test");
    });

Task("__Release")
     .Does(() => {
        TeamCity.WriteStartBlock("Commit Release");
        Npm.RunScript("release");
        TeamCity.WriteEndBlock("Commit Release");
     });



Task("Build")
    .IsDependentOn("__Versioning")
    .IsDependentOn("__NpmInstall")
    .IsDependentOn("__Build")
    .IsDependentOn("__Test")
    .IsDependentOn("__Release");


Task("Default")
  .IsDependentOn("Build");

RunTarget(target);