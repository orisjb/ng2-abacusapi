﻿import { Guid } from '../../types/guid';
import * as moment from 'moment';
import {BaseQuestion} from './base-question';
export namespace StartersQuestion {
   export class IStartersQuestion extends BaseQuestion {
        businessId: number;
        whenFrom: moment.Moment;
        whenTo: moment.Moment;
        constructor() {
            super('Starters/StartersQuestion');
        }
    }

}
export namespace OccupancyQuestion {
   export class IMixedRatioOccupancyOpportunitiesDailyQuestion extends BaseQuestion {
        businessId: number;
        whenFrom: moment.Moment;
        whenTo: moment.Moment;
        constructor() {
            super('Occupancy/MixedRatioOccupancyOpportunitiesDailyQuestion');
        }
    }

   export class IOccupancyOpportunitesDailyQuestion extends BaseQuestion {
        whenFrom: moment.Moment;
        whenTo: moment.Moment;
        businessId: number;
        constructor() {
            super('Occupancy/OccupancyOpportunitesDailyQuestion');
        }
    }

   export class INonMixedRatioOccupancyOpportunitesHourlyQuestion extends BaseQuestion {
        sessionDate: moment.Moment;
        businessId: number;
        constructor() {
            super('Occupancy/NonMixedRatioOccupancyOpportunitesHourlyQuestion');
        }
    }

   export class ITrendOverallYearlyQuestion extends BaseQuestion {
        businessId: number;
        whenFrom: moment.Moment;
        whenTo: moment.Moment;
        constructor() {
            super('Occupancy/TrendOverallYearlyQuestion');
        }
    }

   export class ITrendOverallMonthlyQuestion extends BaseQuestion {
        businessId: number;
        whenFrom: moment.Moment;
        whenTo: moment.Moment;
        constructor() {
            super('Occupancy/TrendOverallMonthlyQuestion');
        }
    }

   export class ITrendOverallWeeklyQuestion extends BaseQuestion {
        businessId: number;
        whenFrom: moment.Moment;
        whenTo: moment.Moment;
        constructor() {
            super('Occupancy/TrendOverallWeeklyQuestion');
        }
    }

   export class ITrendOverallDailyQuestion extends BaseQuestion {
        businessId: number;
        whenFor: moment.Moment;
        constructor() {
            super('Occupancy/TrendOverallDailyQuestion');
        }
    }

}
export namespace LeaversQuestion {
   export class ILeaversQuestion extends BaseQuestion {
        businessId: number;
        whenFrom: moment.Moment;
        whenTo: moment.Moment;
        roomIds: any; // ICollection`1;
        constructor() {
            super('Leavers/LeaversQuestion');
        }
    }

}
export namespace EnquiriesQuestion {
   export class IEnquiryConversionsQuestion extends BaseQuestion {
        whenFrom: moment.Moment;
        whenTo: moment.Moment;
        businessId: number;
        constructor() {
            super('Enquiries/EnquiryConversionsQuestion');
        }
    }

   export class INumberOfEnquiriesQuestion extends BaseQuestion {
        whenFrom: moment.Moment;
        whenTo: moment.Moment;
        businessId: number;
        constructor() {
            super('Enquiries/NumberOfEnquiriesQuestion');
        }
    }

   export class IShowRoundsQuestion extends BaseQuestion {
        whenFrom: moment.Moment;
        whenTo: moment.Moment;
        businessId: number;
        constructor() {
            super('Enquiries/ShowRoundsQuestion');
        }
    }

   export class ISourceOfEnquiriesQuestion extends BaseQuestion {
        whenFrom: moment.Moment;
        whenTo: moment.Moment;
        businessId: number;
        constructor() {
            super('Enquiries/SourceOfEnquiriesQuestion');
        }
    }

}
export namespace ChildrenQuestion {
   export class IAccountBalanceQuestion extends BaseQuestion {
        businessId: number;
        children: any; // IEnumerable`1;
        carerId: number;
        constructor() {
            super('Children/AccountBalanceQuestion');
        }
    }

   export class IAccountHistoryQuestion extends BaseQuestion {
        businessId: number;
        children: any; // IEnumerable`1;
        carerId: number;
        constructor() {
            super('Children/AccountHistoryQuestion');
        }
    }

}
