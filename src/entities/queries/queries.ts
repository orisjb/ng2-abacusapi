﻿import {Guid} from '../../types/guid';
import * as moment from 'moment';
import {IBaseQuery} from './ibase-query';
export interface IUser extends IBaseQuery {
    dateOfBirth: string;
    nurseryCode: string;
    userCode: string;
    password: string;
    settingHasDayshare: boolean;
    settingHasFootsteps: boolean;
    settingHasAbacus: boolean;
    userPermissions: string;
    uniqueId: string;
    nurseryId: number;
    chainId: number;
    emailAddress: string;
    firstName: string;
    lastName: string;
    nurseryName: string;
    businessName: string;
    id: number;
}
export interface IRoom extends IBaseQuery {
    name: string;
    businessId: number;
    isMixedStaff: boolean;
    nonOccupancy: number;
    capacity: number;
    monday: number;
    tuesday: number;
    wednesday: number;
    thursday: number;
    friday: number;
    saturday: number;
    sunday: number;
    id: number;
}
export interface IActivityMedia extends IBaseQuery {
    name: string;
    location: string;
    lookupToken: Guid;
    memberId: number;
    activityStart: moment.Moment;
    childId: number;
    id: number;
}
export interface ILatestChildEvent extends IBaseQuery {
    businessId: string;
    latestEvent: moment.Moment;
    id: number;
}
export interface IEventMedia extends IBaseQuery {
    type: number;
    childId: number;
    name: string;
    location: string;
    eventId: number;
    eventDate: moment.Moment;
    id: number;
}
export interface IChildEvent extends IBaseQuery {
    type: number;
    businessId: string;
    childId: number;
    firstName: string;
    knownName: string;
    lastName: string;
    gender: number;
    eventStart: moment.Moment;
    eventEnd: any; // Nullable`1;
    name: string;
    comment: string;
    status: any; // Nullable`1;
    images: number;
    individualImages: number;
    id: number;
}
export interface IChildActivity extends IBaseQuery {
    businessId: string;
    childId: string;
    firstName: string;
    knownName: string;
    lastName: string;
    gender: number;
    whenStarted: moment.Moment;
    whenEnded: moment.Moment;
    name: string;
    comment: string;
    images: number;
    individualImages: number;
    id: number;
}
export interface IChildArrivalAndDeparture extends IBaseQuery {
    businessId: string;
    childId: string;
    childFirstName: string;
    childKnownName: string;
    childLastName: string;
    sessionDate: moment.Moment;
    arrival: any; // Nullable`1;
    departure: any; // Nullable`1;
    collectionType: any; // Nullable`1;
    contactId: string;
    carerFirstName: string;
    carerLastName: string;
    id: number;
}
export interface IChildMeal extends IBaseQuery {
    businessId: string;
    childId: string;
    firstName: string;
    knownName: string;
    lastName: string;
    gender: number;
    whenStarted: moment.Moment;
    whenEnded: moment.Moment;
    name: string;
    eatenStatus: number;
    comment: string;
    id: number;
}
export interface IChildNappy extends IBaseQuery {
    businessId: string;
    childId: string;
    firstName: string;
    knownName: string;
    lastName: string;
    whenChanged: moment.Moment;
    changeType: number;
    comment: string;
    id: number;
}
export interface IChildNote extends IBaseQuery {
    businessId: string;
    childId: string;
    firstName: string;
    knownName: string;
    lastName: string;
    whenValid: moment.Moment;
    comment: string;
    images: number;
    id: number;
}
export interface IChildObservation extends IBaseQuery {
    businessId: string;
    firstName: string;
    knownName: string;
    lastName: string;
    recordedDate: moment.Moment;
    areaId: number;
    areaAbv: string;
    areaName: string;
    aspectId: number;
    aspectName: string;
    ageBand: string;
    obsId: number;
    description: string;
    images: number;
    isHomeObservation: boolean;
    id: number;
}
export interface IChildSleep extends IBaseQuery {
    businessId: string;
    childId: string;
    firstName: string;
    knownName: string;
    lastName: string;
    whenFrom: moment.Moment;
    whenTo: moment.Moment;
    comment: string;
    id: number;
}
export interface INoteMedia extends IBaseQuery {
    name: string;
    location: string;
    lookupToken: Guid;
    noteId: number;
    noteDate: moment.Moment;
    childId: number;
    id: number;
}
export interface IObservationMedia extends IBaseQuery {
    name: string;
    location: string;
    lookupToken: Guid;
    observationId: number;
    recordedDate: moment.Moment;
    childId: number;
    type: number;
    id: number;
}
export interface IEnquirySource extends IBaseQuery {
    name: string;
    businessId: number;
    id: number;
}
export interface IContact extends IBaseQuery {
    title: string;
    firstName: string;
    lastName: string;
    emailAddress: string;
    homeTelephoneNumber: string;
    mobileTelephoneNumber: string;
    workTelephoneNumber: string;
    id: number;
}
export interface IParentPortalAdult extends IBaseQuery {
    businessId: number;
    title: string;
    firstName: string;
    lastName: string;
    username: string;
    password: string;
    dateOfBirth: string;
    isActive: boolean;
    id: number;
}
export interface IParentPortalChildCarers extends IBaseQuery {
    businessId: number;
    adultId: number;
    name: string;
    relationship: string;
    parentalResponsibility: boolean;
    emergencycontact: boolean;
    activateParentPortal: boolean;
    isParentPortalActive: boolean;
    id: number;
}
export interface IParentPortalCarerChildren extends IBaseQuery {
    businessId: number;
    adultId: number;
    firstName: string;
    middleName: string;
    lastName: string;
    knownName: string;
    dateOfBirth: moment.Moment;
    gender: string;
    relationship: string;
    emergencyContact: boolean;
    isEmergencyHomeNo: boolean;
    isEmergencyOtherNo: boolean;
    isEmergencyWorkNo: boolean;
    startDate: moment.Moment;
    room: string;
    keyWorker: string;
    estimatedLeaveDate: moment.Moment;
    parentalResponsibility: boolean;
    activateParentPortal: boolean;
    isParentPortalActive: boolean;
    id: number;
}
export interface IParentPortalFamilyAdult extends IBaseQuery {
    businessId: number;
    title: string;
    firstName: string;
    lastName: string;
    houseNumber: string;
    addressLine1: string;
    addressLine2: string;
    addressLine3: string;
    town: string;
    county: string;
    postCode: string;
    homeTelephone: string;
    workTelephone: string;
    otherTelephone: string;
    emailAddress: string;
    isEmergencyHomeNo: boolean;
    isEmergencyWorkNo: boolean;
    isEmergencyOtherNo: boolean;
    isEmergencyContact: boolean;
    isParentPortalActive: boolean;
    id: number;
}
export interface IFeePlannerNurseryDetail extends IBaseQuery {
    rowId: Guid;
    bacsReference: string;
    accountName: string;
    accountSortCode: string;
    accountNumber: string;
    email: string;
    title: string;
    forename: string;
    surname: string;
    addressLine1: string;
    addressLine2: string;
    addressLine3: string;
    city: string;
    county: string;
    postCode: string;
    country: string;
    phoneNumber: string;
    alternateKey: string;
    status: number;
    id: number;
}
export interface IBusinessPermissions extends IBaseQuery {
    businessId: number;
    itemName: string;
    id: number;
}
export interface IBusinessIllnesses extends IBaseQuery {
    businessId: number;
    itemName: string;
    id: number;
}
export interface IBusinessMedical extends IBaseQuery {
    businessId: number;
    itemName: string;
    id: number;
}
export interface IBusinessDietary extends IBaseQuery {
    businessId: number;
    itemName: string;
    id: number;
}
export interface IBusinessAllergies extends IBaseQuery {
    businessId: number;
    itemName: string;
    id: number;
}
export interface IBusiness extends IBaseQuery {
    name: string;
    contactName: string;
    contactEmailAddress: string;
    address1: string;
    address2: string;
    address3: string;
    address4: string;
    town: string;
    county: string;
    postcode: string;
    tel: string;
    id: number;
}
export interface ISession extends IBaseQuery {
    businessId: number;
    childId: number;
    groupId: number;
    groupDate: moment.Moment;
    groupStart: moment.Moment;
    groupEnd: moment.Moment;
    sessionStart: moment.Moment;
    sessionEnd: moment.Moment;
    type: number;
    typeName: string;
    roomName: string;
    id: number;
}
export interface IGroupSessionAndHoliday extends IBaseQuery {
    groupId: any; // Nullable`1;
    businessId: number;
    childId: number;
    date: moment.Moment;
    startTime: moment.Moment;
    endTime: moment.Moment;
    type: number;
    typeName: string;
    discount: number;
    id: number;
}
export interface IAppointmentExtraSessionDiaryEvent extends IBaseQuery {
    firstName: string;
    lastName: string;
    knownName: string;
    businessId: number;
    childId: number;
    whenFrom: moment.Moment;
    whenTo: moment.Moment;
    id: number;
}
export interface IAppointmentHeadCountDiaryEvent extends IBaseQuery {
    businessId: number;
    whenFor: moment.Moment;
    subject: string;
    id: number;
}
export interface IAppointmentLeaverDiaryEvent extends IBaseQuery {
    firstName: string;
    lastName: string;
    knownName: string;
    businessId: number;
    whenFor: moment.Moment;
    id: number;
}
export interface IAppointmentStaffAppraisalsDiaryEvent extends IBaseQuery {
    businessId: number;
    whenFor: moment.Moment;
    firstName: string;
    lastName: string;
    id: number;
}
export interface IAppointmentStaffAttendedTrainingDiaryEvent extends IBaseQuery {
    staffId: number;
    businessId: number;
    whenFor: moment.Moment;
    firstName: string;
    lastName: string;
    trainingName: string;
    id: number;
}
export interface IAppointmentStaffExpiredTrainingDiaryEvent extends IBaseQuery {
    staffId: number;
    businessId: number;
    whenFor: moment.Moment;
    firstName: string;
    lastName: string;
    trainingName: string;
    id: number;
}
export interface IAppointmentStaffLeaveDateDiaryEvent extends IBaseQuery {
    businessId: number;
    whenFor: moment.Moment;
    firstName: string;
    lastName: string;
    id: number;
}
export interface IAppointmentStaffPoliceCheckDiaryEvent extends IBaseQuery {
    businessId: number;
    whenFor: moment.Moment;
    firstName: string;
    lastName: string;
    id: number;
}
export interface IAppointmentStaffStartDateDiaryEvent extends IBaseQuery {
    businessId: number;
    whenFor: moment.Moment;
    firstName: string;
    lastName: string;
    id: number;
}
export interface IAppointmentStarterDiaryEvent extends IBaseQuery {
    firstName: string;
    lastName: string;
    knownName: string;
    businessId: number;
    whenFor: moment.Moment;
    id: number;
}
export interface IChildHolidayDiaryEvent extends IBaseQuery {
    childId: number;
    firstName: string;
    lastName: string;
    knownName: string;
    businessId: number;
    whenFor: moment.Moment;
    id: number;
}
export interface ICustomDiaryEvent extends IBaseQuery {
    recurrenceId: number;
    businessId: number;
    subject: string;
    notes: string;
    roomIds: string;
    staffFirstName: string;
    staffLastName: string;
    whenFrom: moment.Moment;
    whenTo: moment.Moment;
    id: number;
}
export interface ICustomDiaryEventRecurrence extends IBaseQuery {
    recurrenceType: any; // RecurrenceType;
    whenFrom: moment.Moment;
    whenTo: moment.Moment;
    businessId: number;
    id: number;
}
export interface INurseryHolidayDiaryEvent extends IBaseQuery {
    name: string;
    whenFrom: moment.Moment;
    whenTo: moment.Moment;
    businessId: number;
    id: number;
}
export interface IStaffBirthdayDiaryEvent extends IBaseQuery {
    firstName: string;
    lastName: string;
    businessId: number;
    whenFor: moment.Moment;
    dayOfYear: number;
    bornInLeapYear: boolean;
    id: number;
}
export interface IChildBirthdayDiaryEvent extends IBaseQuery {
    firstName: string;
    lastName: string;
    knownName: string;
    businessId: number;
    whenFor: moment.Moment;
    dayOfYear: number;
    bornInLeapYear: boolean;
    id: number;
}
export interface IEnquiryDiaryEvent extends IBaseQuery {
    businessId: number;
    whenFor: moment.Moment;
    subject: string;
    id: number;
}
export interface IShowRoundDiaryEvent extends IBaseQuery {
    businessId: number;
    subject: string;
    whenFor: moment.Moment;
    whenFrom: moment.Moment;
    whenTo: moment.Moment;
    id: number;
}
export interface IStaffHolidayDiaryEvent extends IBaseQuery {
    staffId: number;
    firstName: string;
    lastName: string;
    businessId: number;
    whenFrom: moment.Moment;
    whenTo: moment.Moment;
    id: number;
}
export interface IChildRecentRoom extends IBaseQuery {
    businessId: number;
    roomId: number;
    roomName: string;
    mostRecentSession: moment.Moment;
    id: number;
}
export interface IChildPermissions extends IBaseQuery {
    businessId: number;
    permissionId: number;
    permissionName: string;
    id: number;
}
export interface IChildIllnesses extends IBaseQuery {
    businessId: number;
    illnessId: number;
    immunised: boolean;
    had: boolean;
    illnessName: string;
    id: number;
}
export interface IChildMedical extends IBaseQuery {
    businessId: number;
    medicalId: number;
    medicalName: string;
    id: number;
}
export interface IChildDietary extends IBaseQuery {
    businessId: number;
    dietaryId: number;
    dietaryName: string;
    id: number;
}
export interface IChildAllergies extends IBaseQuery {
    businessId: number;
    allergyId: number;
    allergyName: string;
    id: number;
}
export interface IChild extends IBaseQuery {
    firstName: string;
    knownName: string;
    lastName: string;
    id: number;
}
