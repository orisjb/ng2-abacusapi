﻿import { Guid } from '../../types/guid';
import * as moment from 'moment';
import {IAnswer} from './ianswer';
export namespace LeaversAnswer {
   export interface ILeaversAnswer extends IAnswer {
      previousTotal: number;
      currentTotal: number;
   }

}
export namespace StartersAnswer {
   export interface IStartersAnswer extends IAnswer {
      previousTotal: number;
      currentTotal: number;
   }

}
export namespace OccupancyAnswer {
   export interface IMixedRatioOccupancyOpportunitiesDailyAnswer extends IAnswer {
      roomId: number;
      lowerAge: number;
      upperAge: number;
      capacity: number;
      amBooked: number;
      amAvailable: number;
      pmBooked: number;
      pmAvailable: number;
   }

   export interface IOccupancyOpportunitesDailyAnswer extends IAnswer {
      roomId: number;
      capacity: number;
      amBooked: number;
      amAvailable: number;
      pmBooked: number;
      pmAvailable: number;
   }

   export interface INonMixedRatioOccupancyOpportunitesHourlyAnswer extends IAnswer {
      roomId: number;
      hour: number;
      capacity: number;
      booked: number;
      available: number;
   }

   export interface ITrendOverallYearlyAnswer extends IAnswer {
      roomId: number;
      previousPercentage: number;
      currentPercentage: number;
   }

   export interface ITrendOverallMonthlyAnswer extends IAnswer {
      roomId: number;
      previousPercentage: number;
      currentPercentage: number;
   }

   export interface ITrendOverallWeeklyAnswer extends IAnswer {
      roomId: number;
      previousPercentage: number;
      currentPercentage: number;
   }

   export interface ITrendOverallDailyAnswer extends IAnswer {
      roomId: number;
      hour: number;
      percentage: number;
   }

}
export namespace EnquiriesAnswer {
   export interface IEnquiryConversionsAnswer extends IAnswer {
      enquirySourceId: number;
      year: number;
      month: number;
      totalConverted: number;
   }

   export interface INumberOfEnquiriesAnswer extends IAnswer {
      previousTotal: number;
      currentTotal: number;
   }

   export interface IShowRoundsAnswer extends IAnswer {
      previousTotal: number;
      currentTotal: number;
   }

   export interface ISourceOfEnquiriesAnswer extends IAnswer {
      month: number;
      year: number;
      enquirySourceId: number;
      previousTotal: number;
      currentTotal: number;
   }

}
export namespace ChildrenAnswer {
   export interface IAccountBalanceAnswer extends IAnswer {
      balance: number;
   }

   export interface IAccountHistoryAnswer extends IAnswer {
      businessId: number;
      date: moment.Moment;
      typeId: number;
      type: string;
      transactionId: string;
      childId: number;
      childName: string;
      paymentType: string;
      amount: number;
      balance: number;
   }

}
