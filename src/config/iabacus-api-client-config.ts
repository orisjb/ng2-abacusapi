export interface IAbacusApiClientConfig {
    apiUrl: string;
    isInternal: boolean;
    errorFunc: Function;
}
