import { OpaqueToken } from '@angular/core';

export const ABACUSAPI_CLIENT_CONFIG = new OpaqueToken('ABACUSAPI_CLIENT_CONFIG');
