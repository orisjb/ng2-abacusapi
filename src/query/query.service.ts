﻿import { Guid } from '../types/guid';
import * as moment from 'moment';
import {ABACUSAPI_CLIENT_CONFIG} from '../config/abacus-api-client-config';
import {IAbacusApiClientConfig} from '../config/iabacus-api-client-config';
import {Inject, Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {BaseQueryService} from './base-query-service';
import {AuthenticationProxyService} from '../authentication-proxy.service';
@Injectable()
export class UserQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(ABACUSAPI_CLIENT_CONFIG)config: IAbacusApiClientConfig,
        authenticationProxyService: AuthenticationProxyService) {
        super(http, config, authenticationProxyService);
    }
    createUserQuery(): jo {
        return new jo(this.generateReadUrl('User', 'User'));
    }
}
@Injectable()
export class RoomQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(ABACUSAPI_CLIENT_CONFIG)config: IAbacusApiClientConfig,
        authenticationProxyService: AuthenticationProxyService) {
        super(http, config, authenticationProxyService);
    }
    createRoomQuery(): jo {
        return new jo(this.generateReadUrl('Room', 'Room'));
    }
}
@Injectable()
export class NewsfeedQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(ABACUSAPI_CLIENT_CONFIG)config: IAbacusApiClientConfig,
        authenticationProxyService: AuthenticationProxyService) {
        super(http, config, authenticationProxyService);
    }
    createActivityMediaQuery(): jo {
        return new jo(this.generateReadUrl('Newsfeed', 'ActivityMedia'));
    }
    createLatestChildEventQuery(): jo {
        return new jo(this.generateReadUrl('Newsfeed', 'LatestChildEvent'));
    }
    createEventMediaQuery(): jo {
        return new jo(this.generateReadUrl('Newsfeed', 'EventMedia'));
    }
    createChildEventQuery(): jo {
        return new jo(this.generateReadUrl('Newsfeed', 'ChildEvent'));
    }
    createChildActivityQuery(): jo {
        return new jo(this.generateReadUrl('Newsfeed', 'ChildActivity'));
    }
    createChildArrivalAndDepartureQuery(): jo {
        return new jo(this.generateReadUrl('Newsfeed', 'ChildArrivalAndDeparture'));
    }
    createChildMealQuery(): jo {
        return new jo(this.generateReadUrl('Newsfeed', 'ChildMeal'));
    }
    createChildNappyQuery(): jo {
        return new jo(this.generateReadUrl('Newsfeed', 'ChildNappy'));
    }
    createChildNoteQuery(): jo {
        return new jo(this.generateReadUrl('Newsfeed', 'ChildNote'));
    }
    createChildObservationQuery(): jo {
        return new jo(this.generateReadUrl('Newsfeed', 'ChildObservation'));
    }
    createChildSleepQuery(): jo {
        return new jo(this.generateReadUrl('Newsfeed', 'ChildSleep'));
    }
    createNoteMediaQuery(): jo {
        return new jo(this.generateReadUrl('Newsfeed', 'NoteMedia'));
    }
    createObservationMediaQuery(): jo {
        return new jo(this.generateReadUrl('Newsfeed', 'ObservationMedia'));
    }
}
@Injectable()
export class EnquiryQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(ABACUSAPI_CLIENT_CONFIG)config: IAbacusApiClientConfig,
        authenticationProxyService: AuthenticationProxyService) {
        super(http, config, authenticationProxyService);
    }
    createEnquirySourceQuery(): jo {
        return new jo(this.generateReadUrl('Enquiry', 'EnquirySource'));
    }
}
@Injectable()
export class ContactQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(ABACUSAPI_CLIENT_CONFIG)config: IAbacusApiClientConfig,
        authenticationProxyService: AuthenticationProxyService) {
        super(http, config, authenticationProxyService);
    }
    createContactQuery(): jo {
        return new jo(this.generateReadUrl('Contact', 'Contact'));
    }
}
@Injectable()
export class AdultQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(ABACUSAPI_CLIENT_CONFIG)config: IAbacusApiClientConfig,
        authenticationProxyService: AuthenticationProxyService) {
        super(http, config, authenticationProxyService);
    }
    createParentPortalAdultQuery(): jo {
        return new jo(this.generateReadUrl('Adult', 'ParentPortalAdult'));
    }
}
@Injectable()
export class FamilyQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(ABACUSAPI_CLIENT_CONFIG)config: IAbacusApiClientConfig,
        authenticationProxyService: AuthenticationProxyService) {
        super(http, config, authenticationProxyService);
    }
    createParentPortalChildCarersQuery(): jo {
        return new jo(this.generateReadUrl('Family', 'ParentPortalChildCarers'));
    }
    createParentPortalCarerChildrenQuery(): jo {
        return new jo(this.generateReadUrl('Family', 'ParentPortalCarerChildren'));
    }
    createParentPortalFamilyAdultQuery(): jo {
        return new jo(this.generateReadUrl('Family', 'ParentPortalFamilyAdult'));
    }
}
@Injectable()
export class FeePlannerQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(ABACUSAPI_CLIENT_CONFIG)config: IAbacusApiClientConfig,
        authenticationProxyService: AuthenticationProxyService) {
        super(http, config, authenticationProxyService);
    }
    createFeePlannerNurseryDetailQuery(): jo {
        return new jo(this.generateReadUrl('FeePlanner', 'FeePlannerNurseryDetail'));
    }
}
@Injectable()
export class BusinessQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(ABACUSAPI_CLIENT_CONFIG)config: IAbacusApiClientConfig,
        authenticationProxyService: AuthenticationProxyService) {
        super(http, config, authenticationProxyService);
    }
    createBusinessPermissionsQuery(): jo {
        return new jo(this.generateReadUrl('Business', 'BusinessPermissions'));
    }
    createBusinessIllnessesQuery(): jo {
        return new jo(this.generateReadUrl('Business', 'BusinessIllnesses'));
    }
    createBusinessMedicalQuery(): jo {
        return new jo(this.generateReadUrl('Business', 'BusinessMedical'));
    }
    createBusinessDietaryQuery(): jo {
        return new jo(this.generateReadUrl('Business', 'BusinessDietary'));
    }
    createBusinessAllergiesQuery(): jo {
        return new jo(this.generateReadUrl('Business', 'BusinessAllergies'));
    }
    createBusinessQuery(): jo {
        return new jo(this.generateReadUrl('Business', 'Business'));
    }
}
@Injectable()
export class AscarQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(ABACUSAPI_CLIENT_CONFIG)config: IAbacusApiClientConfig,
        authenticationProxyService: AuthenticationProxyService) {
        super(http, config, authenticationProxyService);
    }
    createSessionQuery(): jo {
        return new jo(this.generateReadUrl('Ascar', 'Session'));
    }
    createGroupSessionAndHolidayQuery(): jo {
        return new jo(this.generateReadUrl('Ascar', 'GroupSessionAndHoliday'));
    }
}
@Injectable()
export class DiaryQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(ABACUSAPI_CLIENT_CONFIG)config: IAbacusApiClientConfig,
        authenticationProxyService: AuthenticationProxyService) {
        super(http, config, authenticationProxyService);
    }
    createAppointmentExtraSessionDiaryEventQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'AppointmentExtraSessionDiaryEvent'));
    }
    createAppointmentHeadCountDiaryEventQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'AppointmentHeadCountDiaryEvent'));
    }
    createAppointmentLeaverDiaryEventQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'AppointmentLeaverDiaryEvent'));
    }
    createAppointmentStaffAppraisalsDiaryEventQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'AppointmentStaffAppraisalsDiaryEvent'));
    }
    createAppointmentStaffAttendedTrainingDiaryEventQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'AppointmentStaffAttendedTrainingDiaryEvent'));
    }
    createAppointmentStaffExpiredTrainingDiaryEventQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'AppointmentStaffExpiredTrainingDiaryEvent'));
    }
    createAppointmentStaffLeaveDateDiaryEventQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'AppointmentStaffLeaveDateDiaryEvent'));
    }
    createAppointmentStaffPoliceCheckDiaryEventQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'AppointmentStaffPoliceCheckDiaryEvent'));
    }
    createAppointmentStaffStartDateDiaryEventQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'AppointmentStaffStartDateDiaryEvent'));
    }
    createAppointmentStarterDiaryEventQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'AppointmentStarterDiaryEvent'));
    }
    createChildHolidayDiaryEventQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'ChildHolidayDiaryEvent'));
    }
    createCustomDiaryEventQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'CustomDiaryEvent'));
    }
    createCustomDiaryEventRecurrenceQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'CustomDiaryEventRecurrence'));
    }
    createNurseryHolidayDiaryEventQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'NurseryHolidayDiaryEvent'));
    }
    createStaffBirthdayDiaryEventQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'StaffBirthdayDiaryEvent'));
    }
    createChildBirthdayDiaryEventQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'ChildBirthdayDiaryEvent'));
    }
    createEnquiryDiaryEventQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'EnquiryDiaryEvent'));
    }
    createShowRoundDiaryEventQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'ShowRoundDiaryEvent'));
    }
    createStaffHolidayDiaryEventQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'StaffHolidayDiaryEvent'));
    }
}
@Injectable()
export class ChildQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(ABACUSAPI_CLIENT_CONFIG)config: IAbacusApiClientConfig,
        authenticationProxyService: AuthenticationProxyService) {
        super(http, config, authenticationProxyService);
    }
    createChildRecentRoomQuery(): jo {
        return new jo(this.generateReadUrl('Child', 'ChildRecentRoom'));
    }
    createChildPermissionsQuery(): jo {
        return new jo(this.generateReadUrl('Child', 'ChildPermissions'));
    }
    createChildIllnessesQuery(): jo {
        return new jo(this.generateReadUrl('Child', 'ChildIllnesses'));
    }
    createChildMedicalQuery(): jo {
        return new jo(this.generateReadUrl('Child', 'ChildMedical'));
    }
    createChildDietaryQuery(): jo {
        return new jo(this.generateReadUrl('Child', 'ChildDietary'));
    }
    createChildAllergiesQuery(): jo {
        return new jo(this.generateReadUrl('Child', 'ChildAllergies'));
    }
    createChildQuery(): jo {
        return new jo(this.generateReadUrl('Child', 'Child'));
    }
}

