import {AuthenticationService} from '@parenta/ng2-sauron';
import {Injectable} from '@angular/core';

@Injectable()
export class AuthenticationProxyService {
    constructor(private authenticationService: AuthenticationService) {

    }

    sauronKey(): string {
        return this.authenticationService.generateAuthKey();
    }
}
