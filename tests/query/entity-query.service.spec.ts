import {TestBed, getTestBed} from '@angular/core/testing';
import {HttpModule, XHRBackend} from '@angular/http';
import {MockBackend, MockConnection} from '@angular/http/testing';
import {AbacusApiModule} from '../../index';
import {ABACUSAPI_CLIENT_CONFIG} from '../../src/config/abacus-api-client-config';
import {AscarQueryService} from '../../src/query/query.service';
import {Injector} from '@angular/core';
import {IAbacusApiClientConfig} from '../../src/config/iabacus-api-client-config';
import {AuthenticationProxyService} from '../../src/authentication-proxy.service';
import {LocalStorageService} from 'angular-2-local-storage';

class MockAuthenticationProxyService {

}

class MockLocalStorageService {

}

describe('Service: Query', () => {
    let injector: Injector;
    let backend: MockBackend;
    let translate: AscarQueryService;
    let connection: MockConnection; // this will be set when a new connection is emitted from the backend.

   /*
    constructor(http: Http, @Inject(ABACUSAPI_CLIENT_CONFIG)config: IAbacusApiClientConfig,
    authenticationProxyService: AuthenticationProxyService) {
    super(http, config, authenticationProxyService);
    }
     */

    let abacusApiClientConfig: IAbacusApiClientConfig = {
        apiUrl: 'http://localhost/api/',
        isInternal: false,
        errorFunc: (error: any) => {
            // DO SOMETHING WITH THIS
            console.log(error);
        }
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpModule, AbacusApiModule],
            providers: [
                {provide: XHRBackend, useClass: MockBackend},
                {provide: AuthenticationProxyService, useClass: MockAuthenticationProxyService},
                {provide: LocalStorageService, useClass: MockLocalStorageService},
                {provide: AscarQueryService, useClass: AscarQueryService},
                {provide: ABACUSAPI_CLIENT_CONFIG, useValue: abacusApiClientConfig},
            ]
        });
        injector = getTestBed();
        backend = injector.get(XHRBackend);
        translate = injector.get(AscarQueryService);
        // sets the connection when someone tries to access the backend with an xhr request
        backend.connections.subscribe((c: MockConnection) => connection = c);
    });

    afterEach(() => {
        injector = undefined;
        backend = undefined;
        translate = undefined;
        connection = undefined;
    });

    it('is defined', () => {
        expect(AscarQueryService).toBeDefined();
        expect(translate).toBeDefined();
        expect(translate instanceof AscarQueryService).toBeTruthy();
    });
    it('foo', () =>{
        let query = translate.createGroupSessionAndHolidayQuery();

        let startDate: jo.FilterClause = new jo.FilterClause('Date').ge('2016-11-08T10:39:01+00:00');
        let endDate: jo.FilterClause = new jo.FilterClause('Date').le('2016-11-08T10:39:01+00:00');

        query.andFilter(startDate);
        query.andFilter(endDate);

        let selectedChildIds: number[] = [123,2654,3567,4123,534324];

        let childGroup: jo.PrecedenceGroup;
        selectedChildIds.forEach((child, index) => {
            if (!childGroup) {
                childGroup = new jo.PrecedenceGroup(new jo.FilterClause('ChildId').eq(child));
            } else {
                childGroup.orFilter(new jo.FilterClause('ChildId').eq(child));
            }
        });
        query.filter(childGroup);
        //throw new Error(query.toString());

    })
});
