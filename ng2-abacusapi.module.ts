import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';

import { AuthenticationProxyService } from './src/authentication-proxy.service';

@NgModule({
    imports: [
        CommonModule,
        HttpModule
    ],
    providers: [AuthenticationProxyService],
})
export class AbacusApiModule {

}
